import argparse
import statistics

parser = argparse.ArgumentParser(
    description='Average timeseries CSV data')
parser.add_argument('files', nargs="*",
                    help='CSV files to average')
args = parser.parse_args()

iterations = []
for filename in args.files:
    samples = []
    with open(filename, 'r') as file:
        start = None
        for line in file.readlines():
            line = line.translate({ord(i): None for i in "[]"})
            time, delta, total = map(int, line.split(','))
            if not start:
                start = time

            samples.append((time - start, delta))

    resampled = []
    for t in range(61):
        resampled.append(0)
        for s in samples:
            if s[0] < t*1000:
                resampled[-1] += s[1]
    iterations.append(resampled)

iterations = [[j/60 for j in i] for i in iterations]
averages = map(statistics.mean, zip(*iterations))
stdev = list(map(statistics.pstdev, zip(*iterations)))
for i, v in enumerate(averages):
    print("{},{},{}".format(i,  v, stdev[i]))
