import sys
import json

data = json.load(sys.stdin)

tabs = {}
for node in data:
    if node['type'] == 'tab':
        tabs[node['id']] = [node]
    elif node['z']:
        tabs[node['z']].append(node)
    else:
        for tab in tabs.values():
            tab.append(node)

for tab in tabs.values():
    with open(tab[0]['label']+'.json', 'w') as flow:
        json.dump(tab, flow)
