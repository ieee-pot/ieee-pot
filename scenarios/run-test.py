import threading
import socket
import random
import simplejson as json
import sys
from threading import Thread
from queue import Queue
import queue
import argparse
from itertools import islice
import time
from ttictoc import TicToc
import math
import requests
import logging

parser = argparse.ArgumentParser(
    description='Generate and monitor test data for PoT.')
parser.add_argument('--quiet', '-q', action='store_true',
                    help='Disable printing to the console')
parser.add_argument('--log-items', action='store_true',
                    help='Enable saving items to files')
parser.add_argument('--port', '-p', type=int, default=8887,
                    help='port number to send messages to')
parser.add_argument('--host', type=str, default="127.0.0.1",
                    help='destination hostname or IP to send messages to')
parser.add_argument('--listen', '-l', type=int, default=8888,
                    help='port number to listen on')
parser.add_argument('--no-feedback', dest="no_feedback", action='store_true',
                    help='Disable message feedback - sending in response to reception')
parser.add_argument('--min-rate', dest="min_rate", type=int, default=1,
                    help='minimum messages per second, or 0 to disable')
parser.add_argument('--max-rate', dest="max_rate", type=int, default=0,
                    help='Maximum messages per second, or 0 to disable')
parser.add_argument('--buffer', '-b', type=int, default=100,
                    help='initial buffer of messages to send')
parser.add_argument('--max-order', type=int, default=4,
                    help='maximum number of items per order')
parser.add_argument('--max-items', dest="max_items", type=int, default=0,
                    help='Maximum total number of items; 0 for unlimited')
parser.add_argument('--min-duration', type=int, default=1,
                    help='minimum runtime in seconds')
parser.add_argument('--max-duration', type=int, default=300,
                    help='Maximum runtime in seconds, 0 for indefinite')
parser.add_argument('--scramble', '-s', type=int, default=0,
                    help='buffer size for scrambling items (or not, if set to 0)')
parser.add_argument('--pulse', type=int, default=1000,
                    help='Frequency to print stats, in #items')
args = parser.parse_args()

source_time = TicToc()

args.buffer = min(
    args.buffer, args.max_items) if args.max_items else args.buffer

global_time = TicToc()
global_time.tic()

exit_q = Queue()
feedback_q = Queue()
min_q = Queue()

results = logging.getLogger('results')
file_handler = logging.FileHandler('results.log')
formatter = logging.Formatter('%(msg)s')
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
results.addHandler(file_handler)


def message(msg):
    if not args.quiet:
        print(msg)


def gen_items():
    oID = 0
    iID = 0
    while True:
        items = random.choices(
            ['shoe', 'ball', 'plate', 'glass'], k=random.randint(1, args.max_order))
        address = random.choice(
            ['Lancaster', 'Preston', 'London', 'Greenwich', 'Oxford'])
        for item in items:
            yield {"oID": oID, "iID": iID, "item": item, "address": address}
            iID += 1
        oID += 1


def scramble(gen, buffer_size):
    buf = []
    i = iter(gen)
    while True:
        try:
            e = next(i)
            buf.append(e)
            if len(buf) >= buffer_size:
                choice = random.randint(0, len(buf)-1)
                buf[-1], buf[choice] = buf[choice], buf[-1]
                yield buf.pop()
        except StopIteration:
            random.shuffle(buf)
            yield from buf
            return


def send_item(item, sent_file):
    t = source_time

    requests.post('http://localhost:1880/items', data=item)

    if args.log_items:
        sent_file.write("{} - {}\n".format(time.time(), json.dumps(item)))
    if (item["iID"] % args.pulse == 0):
        t.toc()
        t.tic()
        if t.elapsed:
            message("\rSent {} items. Time: {}, Rate: {}"
                    .format(item["iID"], t.elapsed, args.pulse/t.elapsed))


def source(exit_q, feedback_q, min_q):
    with open("sent", "w") as sent_file:
        items = scramble(gen_items(), args.scramble)
        count = 0
        for i in islice(items, 0, args.buffer):
            send_item(i, sent_file)
            count += 1
        print("finished buffer: {} items, {}s , {} items/s".format(args.buffer,
                                                                   global_time.toc(), args.buffer/global_time.elapsed))

        t = TicToc()
        t.elapsed = 0

        if args.max_items and args.max_items > args.buffer:
            items = islice(items, 0, args.max_items - args.buffer)
        elif args.max_items:
            items = []
        for i in items:
            # wait until we get the go ahead
            t.tic()
            if args.min_rate:
                try:
                    feedback_q.get(True, timeout=(1/args.min_rate))
                except queue.Empty:
                    pass
            try:
                signal = exit_q.get(False)
                if signal == 'exit':
                    exit_q.put('exit')
                    break
            except queue.Empty:
                pass
            send_item(i, sent_file)
            count += 1
            t.toc()
            if args.max_rate > 0 and t.elapsed < 1/args.max_rate:
                time.sleep(1/args.max_rate - t.elapsed)
        min_q.put('done')
        if args.quiet:
            print("{},{},{},{}".format("sent", global_time.toc(), int(
                count), count / global_time.elapsed))


received = 0
@post('/items')
def receive():
    global received
    if received == 0:
        message("Received first item: {}".format(json.dumps(request.json)))
        results.info("{},{},{},{}".format("action", time.time(), 0, 0))
    received += 1
    if (args.max_items and received == args.max_items):
        results.info("{},{},{},{}".format("received", global_time.toc(),
                                          received, received / global_time.elapsed))
        exit_q.put('exit')
    feedback_q.put('received')
    if (received % args.pulse == 0):
        message("Received - items: {}. time: {}, rate: {}".format(
            received, global_time.toc(), received/global_time.elapsed))
        results.info("{},{},{},{}".format("received", global_time.elapsed,
                                          received, received / global_time.elapsed))


def min_timer(exit_q, min_q):
    t = time.time()
    while time.time()-t < args.min_duration:
        time.sleep(1)
        try:
            signal = exit_q.get(False)
            if signal == 'exit':
                exit_q.put('exit')
                break
        except queue.Empty:
            pass
    min_q.get()
    exit_q.put('exit')


def max_timer(exit_q):
    t = time.time()
    while time.time()-t < args.max_duration:
        time.sleep(1)
        try:
            signal = exit_q.get(False)
            if signal == 'exit':
                exit_q.put('exit')
                break
        except queue.Empty:
            pass
    exit_q.put('exit')


def thunk(exit_q):
    pass


threads = [
    Thread(target=run, kwargs={"port": args.listen, "quiet": True}),
    Thread(target=source, args=(exit_q, feedback_q, min_q)),
    Thread(target=min_timer, args=(exit_q, min_q)),
    Thread(target=max_timer if args.max_duration else thunk, args=(exit_q, )),
]
threads[0].daemon = True
for t in threads:
    t.start()
