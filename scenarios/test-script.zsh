mkdir -p results
sudo -s -H <<'EOF'
source ./test-funcs.sh

loss_loop protocol-udp/protocol-flows.json "protocol-udp"
loss_loop protocol-retry/retry-flows.json "protocol-retry"
broker=1 loss_loop protocol-mqtt/protocol-flows.json "protocol-mqtt"
broker=1 loss_loop protocol-mqtt-qos0/protocol-flows.json "protocol-mqtt-qos0"

EOF
