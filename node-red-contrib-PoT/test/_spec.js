var should = require("should");
var helper = require("node-red-node-test-helper");
var bsplNode = require("../PoT-outgoing.js");

helper.init(require.resolve('node-red'));

describe('PoT-outgoing node: ', function () {
  beforeEach(function (done) {
    // helper.startServer(done);
    done()
  });

  afterEach(function (done) {
    helper.unload();
    // helper.stopServer(done);
    done()
  });

  it('simple test', function(done) {
    var flow = [{ id: "n1", type:'PoT-outgoing', name: "test name" }];
    helper.load(bsplNode, flow, function () {
      var n1 = helper.getNode("n1");
      n1.should.have.property('name', 'test name');
      done();
    });
  })

  it('should be loaded', function (done) {
    var flow = [{ id: "n1", type: "PoT-outgoing", name: "test name" }];
    helper.load(bsplNode, flow, function () {
      var n1 = helper.getNode("n1");
      console.log(n1)
      n1.should.have.property('name', 'test name');
      done();
    });
  });

  it('should validate simple message', function (done) {
    var flow = [{ id: "n1", type: "PoT-outgoing", name: "test name", spec:"out id key", wires:[["n2"]] },
    { id: "n2", type: "helper" }];
    helper.load(bsplNode, flow, function () {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function (msg) {
        msg.should.have.property('payload', {'id':'test'});
        done();
      });
      n1.receive({ payload: {id: 'test'} });
    });
  });
});
