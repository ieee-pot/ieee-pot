const { DEBUG, parseParams, getEnactment, observe, getPredecessors, bindings, debug } = require('./util')

function cleanupHistory(timeout, history) {
  var time = Date.now();
  var msg = null;
  for (var key of Object.keys(history).filter(k => k != 'messages')) {
    for (var enactment of Object.keys(history[key])) {
      let latest = history[key][enactment].slice(-1)[0] // last message in list
      if (latest && latest._timestamp + timeout < time) {
        delete history[key][enactment]
      }
    }
  }
}

function validReception(spec, history, msg) {
  payload = msg.payload
  // find relevant predecessors in history
  preds = getPredecessors(spec.keys, history, payload)
  if (preds.filter(p => _.isEqual(_.omitBy(p,(v,k)=>k[0]=='_'), payload)).length) {
    debug("duplicate message")
    // second slot is duplicate
    return [null, msg]
  }
  for (var p of spec.parameters.filter(p => !spec.keys.includes(p))) {
    // only bother with parameters included in message
    if (payload[p] !== undefined) {
      // reject parameters that are supposed to be produced by this role
      if (spec.outs.includes(p)) {
        debug("Incoming message conflicts with role authority on: out " + p)
        return false
      }
      // all parameters should satisfy the key constraints
      bs = bindings(p, preds)
      if (bs.length > 0 && bs.filter(v => v != payload[p]).length) {
        debug("Message does not satisfy key constraints for parameter: " + p)
        return false
      }
    }
  }
  // first slot is new
  return [msg, null]
}

module.exports = function(RED) {
  function BsplIncomingNode(config) {
    RED.nodes.createNode(this,config);

    let flow = this.context().flow
    let history = flow.get("history")
    if (history == undefined) {
      history = {"parameters": {}}
      flow.set('history', history)
    }

    if (config.spec != "")
      this.spec = parseParams(config.spec)

    if (config.timeout != "")
      this.timeout = Number(config.timeout)
    else
      this.timeout = 0 // No timeout

    var node = this;

    if (config.timeout > 0) {
      debug("setting timeout: ", config.timeout)
      node.interval = setInterval(function () {
        cleanupHistory(node.timeout, flow.get("history"))
      }, 1000)
    }

    node.on('input', function(msg, send, done) {
      payload = msg.payload

      msg = validReception(node.spec, history, msg)
      if (!msg) {
        if (DEBUG) this.log("Invalid reception: " + JSON.stringify(payload))
        // reject invalid messages
        if (done) { done(); return } else { return }
      } else if(msg[0]) { // only for new messages
        observe(node.spec, history, payload)
        if (DEBUG) this.log("Observed: " + JSON.stringify(payload))
      }
      flow.set("history", history)

      // attach enactment to message, whether duplicate or new
      msg.forEach(m => m && (m.enactment = getEnactment(node.spec.keys, history, payload)))
      node.send(msg)

      if (done) { done() }
    });

    node.on('close', function() {
      // tidy up any state
      flow.set('history', undefined)
    });
  }
  RED.nodes.registerType("PoT-incoming",BsplIncomingNode);
}
