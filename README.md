# Protocols over Things

A prototype implementation of a decentralized programming model for IoT applications.

## Installation

This project contains code for two custom Node-RED nodes, PoT-incoming and PoT-outgoing.

To install them, run the following in your `~/.node-red` directory:
```
$ npm install ~/path/to/PoT/node-red-contrib-PoT
```

## Running
To run the example flows in this repository, install NodeRED and then run the following command in the directory:
```
$ node-red flows.json
```
